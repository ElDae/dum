package pl.szypula.dum.manager;

import android.util.Pair;

import javax.inject.Inject;

import pl.szypula.dum.App;
import pl.szypula.dum.AppConfig;
import pl.szypula.dum.db.facade.ItemFacade;
import pl.szypula.dum.db.facade.MediaFacade;
import pl.szypula.dum.db.model.Item;
import pl.szypula.dum.network.RestAdapterInvoker;
import pl.szypula.dum.network.response.reddit.Child;
import pl.szypula.dum.network.response.reddit.InfoResponse;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class RedditManager extends BaseFeedManager<RedditAccount> {

    @Inject
    public RedditManager(App app, ItemFacade itemFacade, MediaFacade mediaFacade, RestAdapterInvoker restAdapterInvoker) {
        super(app, itemFacade, mediaFacade, restAdapterInvoker);
        accounts.add(new RedditAccount(AppConfig.R_TIL_ID));
    }

    @Override
    protected void fetchFeed(boolean sendCompleteEvent, boolean newest, RedditAccount account) {
        Timber.d("fetchFeed " + account.getName());
        account.setOldestItem(null);
        request = restAdapterInvoker.getRedditRestInterface().subredditInfo(account.getId())
                .flatMap(infoResponse -> prepareSubredditFeedRequest(infoResponse, newest, account))
                .flatMap(stringStringPair -> restAdapterInvoker.getRedditRestInterface().subredditFeed(stringStringPair.first, stringStringPair.second, null, AppConfig.REDDIT_ORDER, AppConfig.ITEM_FETCH_LIMIT))
                .doOnNext(infoResponse -> account.setWasEmptyResponse(infoResponse.getData().getChildren().size() == 0))
                .flatMap(infoResponse -> Observable.from(infoResponse.getData().getChildren()))
                .filter(Child::isValidItem)
                .doOnNext(child -> this.saveItemInDatabase(child, account))
                .doOnCompleted(() -> this.handleResponse(account))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(child -> {
                }, throwable -> afterFetchFeed(sendCompleteEvent, throwable), () -> afterFetchFeed(sendCompleteEvent, null));
    }

    @Override
    protected void fetchMoreItemsForAccountIfNeeded(boolean newest, RedditAccount account) {
        Timber.d("fetchMoreItemsForRedditAccountIfNeeded " + account.getName() + ", " + newest);
        itemFacade.getCountOfUnseenItemsForFeedObservable(Item.FEED_REDDIT, account.getName())
                .subscribeOn(Schedulers.newThread())
                .filter(unseenCount -> unseenCount < AppConfig.MIN_UNSEEN_COUNT)
                .subscribe(shouldFetchMoreItems -> fetchFeed(false, newest, account), throwable -> {
                }, () -> {
                });
    }

    public Observable<Pair<String, String>> prepareSubredditFeedRequest(InfoResponse infoResponse, boolean newest, RedditAccount redditAccount) {
        Timber.d("prepareSubredditFeedRequest");
        String displayName = infoResponse.getUrl().split("/")[2];
        redditAccount.setName(displayName);
        return Observable.zip(Observable.just(displayName),
                itemFacade.getItemToStartDownloadFromForFeedObservable(!newest, Item.FEED_REDDIT, displayName),
                Observable.just(newest),
                this::mapToDisplayNameIdPair); // Android Studio thinks this line will cause compilation error, but it is correct
    }

    public Pair<String, String> mapToDisplayNameIdPair(String displayName, Item item, Boolean newest) {
        String ItemExternalId = null;
        if (!newest && item != null) {
            Timber.d(item.toString());
            ItemExternalId = AppConfig.REDDIT_ITEM_PREFIX + item.getExternalId();
        }
        return new Pair<>(displayName, ItemExternalId);
    }

}
