package pl.szypula.dum.manager;

public class TwitterAccount extends BaseAccount {

    public TwitterAccount(String name, String id) {
        this.name = name;
        this.id = id;
        this.wasEmptyResponse = false;
        this.oldestItem = null;
    }
}
