package pl.szypula.dum.manager;

public class RedditAccount extends BaseAccount {

    public RedditAccount(String id) {
        this.id = id;
        this.wasEmptyResponse = false;
        this.oldestItem = null;
    }
}
