package pl.szypula.dum.manager;

import pl.szypula.dum.db.model.Item;

public class BaseAccount {

    protected String id;
    protected String name;
    protected Boolean wasEmptyResponse;
    protected Item oldestItem;
    protected int failCounter;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getWasEmptyResponse() {
        return wasEmptyResponse;
    }

    public void setWasEmptyResponse(Boolean wasEmptyResponse) {
        this.wasEmptyResponse = wasEmptyResponse;
    }

    public Item getOldestItem() {
        return oldestItem;
    }

    public void setOldestItem(Item oldestItem) {
        this.oldestItem = oldestItem;
    }

    public int getFailCounter() {
        return failCounter;
    }

    public void setFailCounter(int failCounter) {
        this.failCounter = failCounter;
    }
}
