package pl.szypula.dum.manager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import pl.szypula.dum.App;
import pl.szypula.dum.db.facade.ItemFacade;
import pl.szypula.dum.db.facade.MediaFacade;
import pl.szypula.dum.db.model.Item;
import pl.szypula.dum.db.model.Media;
import pl.szypula.dum.event.FetchFeedCompletedEvent;
import pl.szypula.dum.network.RestAdapterInvoker;
import pl.szypula.dum.network.response.reddit.Child;
import pl.szypula.dum.network.response.twitter.Tweet;
import pl.szypula.dum.network.response.twitter.Urls;
import rx.Subscription;
import timber.log.Timber;

public abstract class BaseFeedManager<T extends BaseAccount> {
    protected App app;
    protected ItemFacade itemFacade;
    protected MediaFacade mediaFacade;
    protected RestAdapterInvoker restAdapterInvoker;
    protected List<T> accounts;
    protected Subscription request;

    public BaseFeedManager(App app, ItemFacade itemFacade, MediaFacade mediaFacade, RestAdapterInvoker restAdapterInvoker) {
        this.app = app;
        this.itemFacade = itemFacade;
        this.mediaFacade = mediaFacade;
        this.restAdapterInvoker = restAdapterInvoker;

        accounts = new ArrayList<>();
    }

    public void fetchFeeds(boolean sendCompleteEvent, boolean newest) {
        app.setRequestInProgress(true);
        for (T account : accounts) {
            fetchFeed(sendCompleteEvent, newest, account);
        }
    }

    protected void saveItemInDatabase(Object object, T account) {
        Timber.d("saveItemInDatabase");
        try {
            Item item = null;
            if (object instanceof Child) {
                item = new Item(((Child) object));
            } else if (object instanceof Tweet) {
                item = new Item(((Tweet) object));

            }
            if (item != null) {
                Timber.d("item: " + item);
                boolean isNewItem = itemFacade.create(item);
                if (isNewItem) {
                    if (object instanceof Tweet) {
                        createMediaForTweet(item, ((Tweet) object));
                    }
                    if (account.getOldestItem() == null || item.getCreation() < account.getOldestItem().getCreation()) {
                        account.setOldestItem(item);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void createMediaForTweet(Item item, Tweet tweet) throws SQLException {
        Timber.d("createMediaForTweet");
        if (tweet.getEntities() != null) {
            if (tweet.getEntities().getMedia() != null){
                for (pl.szypula.dum.network.response.twitter.Media media : tweet.getEntities().getMedia()) {
                    Media dbMedia = new Media(item, media);
                    mediaFacade.createOrUpdate(dbMedia);
                    Timber.d("media: " + dbMedia.toString());
                }
            }
            if (tweet.getEntities().getUrls() != null) {
                for (Urls urls : tweet.getEntities().getUrls()) {
                    Media dbMedia = new Media(item, urls);
                    mediaFacade.createOrUpdate(dbMedia);
                    Timber.d("media: " + dbMedia.toString());
                }
            }
        }
    }

    protected void handleResponse(T account) {
        if (!account.wasEmptyResponse) {
            markOldestFetchedItem(account);
            fetchMoreItemsForAccountIfNeeded(false, account);
        }
    }

    protected void markOldestFetchedItem(T account) {
        Timber.d("markOldestFetchedItem");
        if (account.getOldestItem() != null) {
            try {
                Item item = itemFacade.getByExternalId(account.getOldestItem().getExternalId());
                item.setMarked(true);
                itemFacade.createOrUpdate(item);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    protected void afterFetchFeed(boolean sendCompleteEvent, Throwable e) {
        Timber.d("afterFetchFeed, send = " + sendCompleteEvent + ", exception = " + e);
        cleanUpAfterRequest();
        if (sendCompleteEvent) {
            EventBus.getDefault().post(new FetchFeedCompletedEvent(e));
        }
    }

    protected void cleanUpAfterRequest() {
        app.setRequestInProgress(false);
        //request.unsubscribe(); // TODO unsubscribe logic
    }

    protected abstract void fetchFeed(boolean sendCompleteEvent, boolean newest, T account);

    protected abstract void fetchMoreItemsForAccountIfNeeded(boolean newest, T account);
}
