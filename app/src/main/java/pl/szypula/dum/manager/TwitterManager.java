package pl.szypula.dum.manager;

import android.util.Pair;

import java.sql.SQLException;

import javax.inject.Inject;

import pl.szypula.dum.App;
import pl.szypula.dum.AppConfig;
import pl.szypula.dum.db.facade.ItemFacade;
import pl.szypula.dum.db.facade.MediaFacade;
import pl.szypula.dum.db.model.Item;
import pl.szypula.dum.network.RestAdapterInvoker;
import pl.szypula.dum.network.response.twitter.TwitterTokenResponse;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class TwitterManager extends BaseFeedManager<TwitterAccount> {

    private boolean newTwitterBearerToken = false;

    private static final String TWITTER_BEARER_TOKEN = "S2dISTVUQzhVam9iZEpDbmx2cEtnMGxNVzpuVURlWnE2cjJmZkpiZWNQdWphWG9ZT3RkQ3NaMEttcUpvYUhIdlVza2ZVUWVja3ZtSA==";
    private static final String HEADER_AUTHORIZATION = "Basic " + TWITTER_BEARER_TOKEN;
    private static final String HEADER_CONTENT_TYPE = "application/x-www-form-urlencoded;charset=UTF-8";
    private static final String GRANT_TYPE = "client_credentials";
    private static final String BEARER_PREFIX = "Bearer ";

    @Inject
    public TwitterManager(App app, ItemFacade itemFacade, MediaFacade mediaFacade, RestAdapterInvoker restAdapterInvoker) {
        super(app, itemFacade, mediaFacade, restAdapterInvoker);
        accounts.add(new TwitterAccount(AppConfig.TWITTER_GOOGLE_FACTS_SCREEN_NAME, AppConfig.TWITTER_GOOGLE_FACTS_USER_ID));
        accounts.add(new TwitterAccount("Know", "221452291"));
    }

    @Override
    protected void fetchFeed(boolean sendCompleteEvent, boolean newest, TwitterAccount account) {
        Timber.d("fetchFeed " + account.getName() + ", failedCounter=" + account.getFailCounter());
        if (!newest && account.getOldestItem() == null) {
            if (account.getFailCounter() > 3) {
                tryToFixFeedForAccount(account);
            }
            if (account.getFailCounter() == 8) { // stop if we failed 8 times
                return;
            }
            account.setFailCounter(account.getFailCounter() + 1);
        }
        account.setOldestItem(null);
        request = app.getTwitterBearerObservable()
                .flatMap(this::getTwitterTokenResponseObservable)
                .map(TwitterTokenResponse::getAccessToken)
                .doOnNext(this::saveBearerToken)
                .flatMap(token ->
                        Observable.zip(itemFacade.getItemToStartDownloadFromForFeedObservable(!newest, Item.FEED_TWITTER, account.getName()),
                                Observable.just(token),
                                Observable.just(newest),
                                this::prepareRequest)) // Android Studio thinks this line will cause compilation error, but it is correct
                .flatMap(stringLongPair ->
                        restAdapterInvoker.getTwitterRestInterface().getTimeLine(BEARER_PREFIX + stringLongPair.first,
                                account.getId(),
                                account.getName(),
                                AppConfig.ITEM_FETCH_LIMIT, stringLongPair.second))
                .doOnNext(tweets -> account.setWasEmptyResponse((tweets.size() == 0)))
                .flatMap(Observable::from)
                .doOnNext(tweet -> saveItemInDatabase(tweet, account))
                .doOnCompleted(() -> handleResponse(account))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tweet -> {
                }, throwable -> afterFetchFeed(sendCompleteEvent, throwable), () -> afterFetchFeed(sendCompleteEvent, null));
    }

    // creation date for tweets sometimes gets screwed up, no idea why
    private void tryToFixFeedForAccount(TwitterAccount account) {
        Timber.d("tryToFixFeedForAccount " + account.getName());
        try {
            itemFacade.delete(itemFacade.getOldestItemForFeed(Item.FEED_TWITTER, account.getName()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void fetchMoreItemsForAccountIfNeeded(boolean newest, TwitterAccount twitterAccount) {
        Timber.d("fetchMoreItemsForAccountIfNeeded " + twitterAccount.getName() + ", " + newest);
        itemFacade.getCountOfUnseenItemsForFeedObservable(Item.FEED_TWITTER, twitterAccount.getName())
                .subscribeOn(Schedulers.newThread())
                .filter(unseenCount -> unseenCount < AppConfig.MIN_UNSEEN_COUNT)
                .subscribe(shouldFetchMoreItems -> fetchFeed(false, newest, twitterAccount), throwable -> {
                }, () -> {
                });
    }

    private void saveBearerToken(String token) {
        if (newTwitterBearerToken) {
            app.getSharedPreferences().edit().putString(AppConfig.PREFS_TWITTER_BEARER, token).apply();
        }
    }

    private Observable<TwitterTokenResponse> getTwitterTokenResponseObservable(String token) {
        if (token != null) {
            return Observable.just(new TwitterTokenResponse(token));
        } else {
            newTwitterBearerToken = true;
            return restAdapterInvoker.getTwitterRestInterface().getBearerToken(HEADER_AUTHORIZATION, HEADER_CONTENT_TYPE, GRANT_TYPE);
        }
    }

    private Pair<String, Long> prepareRequest(Item item, String token, Boolean newest) {
        Timber.d("prepareTweeterRequest");
        Long ItemExternalId = null;
        if (!newest && item != null) {
            Timber.d(item.toString());
            ItemExternalId = Long.valueOf(item.getExternalId());
        }
        return new Pair<>(token, ItemExternalId);
    }

}
