package pl.szypula.dum.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;
import pl.szypula.dum.App;
import pl.szypula.dum.R;
import pl.szypula.dum.db.facade.ItemFacade;
import pl.szypula.dum.util.FragmentNavigationUtil;

public abstract class BaseActivity extends ActionBarActivity {

    @Inject App app;
    @Inject ItemFacade itemFacade; // FIXME: // unused but without it throws NoSuchFieldError. Why Dagger, why?
    @Optional @InjectView(R.id.toolbar) Toolbar toolbar;

    protected FragmentNavigationUtil fragmentNavigationUtil;
    protected Handler backPressHandler = new Handler();
    protected boolean doubleBackToExitPressedOnce;
    protected final Runnable backPressRunnable = new Runnable() {
        @Override
        public void run() {
            doubleBackToExitPressedOnce = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((App) getApplication()).inject(this);
        setContentView(getLayoutId());
        ButterKnife.inject(this);
        Picasso.with(this).setIndicatorsEnabled(true);
        app.setCurrentActivity(this);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        fragmentNavigationUtil = new FragmentNavigationUtil(getSupportFragmentManager());

        afterOnCreate();
    }

    protected abstract int getLayoutId();

    protected void afterOnCreate() {
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce || getSupportFragmentManager().getBackStackEntryCount() > 1) {
            if (doubleBackToExitPressedOnce) {
                getSupportFragmentManager().popBackStack(null, android.support.v4.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                super.onBackPressed();
            } else {
                finish();
            }
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.exit_msg), Toast.LENGTH_SHORT).show();
        backPressHandler.postDelayed(backPressRunnable, 2000);
    }
}
