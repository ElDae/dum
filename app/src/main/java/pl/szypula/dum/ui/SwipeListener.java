package pl.szypula.dum.ui;

import android.view.GestureDetector;
import android.view.MotionEvent;

public class SwipeListener extends GestureDetector.SimpleOnGestureListener {

    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;

    private OnSwipeListener onSwipeListener;

    public SwipeListener(OnSwipeListener onSwipeListener) {
        this.onSwipeListener = onSwipeListener;
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if (Math.abs(e1.getY() - e2.getY()) <= SWIPE_MAX_OFF_PATH) {
            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                onSwipeListener.onSwipe(Direction.LEFT);
            } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                onSwipeListener.onSwipe(Direction.RIGHT);
            }
        } else if (Math.abs(e1.getX() - e2.getX()) <= SWIPE_MAX_OFF_PATH) {
            if (e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
                onSwipeListener.onSwipe(Direction.UP);
            } else if (e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
                onSwipeListener.onSwipe(Direction.DOWN);
            }
        }

        return super.onFling(e1, e2, velocityX, velocityY);
    }

    public static interface OnSwipeListener {
        void onSwipe(Direction direction);
    }

    public enum Direction {
        LEFT, RIGHT, UP, DOWN
    }

}
