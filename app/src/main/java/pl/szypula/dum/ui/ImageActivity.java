package pl.szypula.dum.ui;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import butterknife.InjectView;
import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import pl.szypula.dum.R;

public class ImageActivity extends BaseActivity {

    @InjectView(R.id.image_image) ImageViewTouch imageView;
    public static final String EXTRA_IMAGE = "ImageActivity:image";

    @Override
    protected int getLayoutId() {
        return R.layout.activity_image;
    }

    @Override
    protected void afterOnCreate() {
        ViewCompat.setTransitionName(imageView, EXTRA_IMAGE);
        Picasso.with(this)
                .load(getIntent().getStringExtra(EXTRA_IMAGE))
                .error(android.R.drawable.ic_dialog_alert)
                .fit().centerInside()
                .into(imageView);
    }

    public static void launch(BaseActivity activity, View transitionView, String url) {
        ActivityOptionsCompat options =
                ActivityOptionsCompat.makeSceneTransitionAnimation(
                        activity, transitionView, EXTRA_IMAGE);
        Intent intent = new Intent(activity, ImageActivity.class);
        intent.putExtra(EXTRA_IMAGE, url);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
