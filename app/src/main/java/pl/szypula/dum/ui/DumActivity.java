package pl.szypula.dum.ui;

import android.widget.ImageView;

import pl.szypula.dum.R;

public class DumActivity extends BaseActivity implements DumFragment.ActivityCallbacks {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_dum;
    }

    @Override
    protected void afterOnCreate() {
        fragmentNavigationUtil.showDumFragment();
    }

    @Override
    public void showImage(ImageView imageView, String src) {
        ImageActivity.launch(DumActivity.this, imageView, src);
    }
}
