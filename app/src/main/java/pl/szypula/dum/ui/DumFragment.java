package pl.szypula.dum.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTouch;
import icepick.Icicle;
import pl.szypula.dum.AppConfig;
import pl.szypula.dum.R;
import pl.szypula.dum.db.model.Item;
import pl.szypula.dum.db.model.Media;
import pl.szypula.dum.event.FetchFeedCompletedEvent;
import pl.szypula.dum.ui.presenter.DumPresenter;
import pl.szypula.dum.ui.presenter.interfaces.DumPresenterInterface;
import pl.szypula.dum.util.AnimationUtil;
import timber.log.Timber;

public class DumFragment extends BaseFragment implements SwipeListener.OnSwipeListener,
        DumPresenterInterface {

    @InjectView(R.id.dum_fragment_content_view1) View contentView1;
    @InjectView(R.id.dum_fragment_content_view2) View contentView2;
    @InjectView(R.id.dum_text_view1) TextView textView1;
    @InjectView(R.id.dum_text_view2) TextView textView2;
    @InjectView(R.id.dum_image_view1) ImageView imageView1;
    @InjectView(R.id.dum_image_view2) ImageView imageView2;
    @InjectView(R.id.dum_feed_name) TextView feedNameTextView;
    @InjectView(R.id.dum_source) TextView sourceTextView;

    @Icicle Long itemId;
    @Inject DumPresenter presenter;
    @Inject AnimationUtil animationUtil;

    private GestureDetector gestureDetector;
    private Animation entryAnimation;
    private Animation exitAnimation;
    private boolean shouldAnimate = true;

    private ActivityCallbacks activityCallbacks;

    public DumFragment() {
    }

    public static DumFragment newInstance() {
        return new DumFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.activityCallbacks = ((ActivityCallbacks) activity);
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement " + DumFragment.class.getSimpleName() + ".ActivityCallbacks");
        }
    }

    @Override
    protected void onAfterCreateView() {
        presenter.setPresenterInterface(this);
    }

    @Override
    protected void onAfterActivityCreated(@Nullable Bundle savedInstanceState) {
        gestureDetector = new GestureDetector(getActivity().getApplicationContext(), new SwipeListener(this));
        entryAnimation = animationUtil.getSlideInLeftToRightAnimation();
        exitAnimation = animationUtil.getSlideOutLeftToRightAnimation();

        if (itemId != null && itemId != 0) {
            reloadItem(itemId);
        } else {
            loadNewItem();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_dum;
    }

    @Override
    protected boolean shouldSaveInstanceState() {
        return (itemId != null);
    }

    private void reloadItem(Long itemId) {
        shouldAnimate = false;
        presenter.reloadItem(itemId);
        loadItemDataIntoView(presenter.getContent());
    }

    private void loadNewItem() {
        shouldAnimate = true;
        itemId = presenter.loadNewRandomItem();
        loadItemDataIntoView(presenter.getContent());

    }

    private void loadItemDataIntoView(Item item) {
        if (item != null) {
            loadContent(item);
            loadSource(item.getSrc());
            loadFeed(item.getFeed(), item.getSubFeed());
        }
    }

    private void loadContent(Item item) {
        String content = item.getContent();
        String imageUrl = null;
        if (item.getMedia() != null && !item.getMedia().isEmpty()) {
            for (Media media : item.getMedia()) {
                if (media.getType().equals(Media.TYPE_PHOTO)) {
                    content = content.replace(media.getUrl(), "");
                    imageUrl = media.getMediaUrl();
                } else if (media.getType().equals(Media.TYPE_URL)) {
                    content = content
                            .replace(media.getUrl(), getActivity().getResources().getString(R.string.hyperlink))
                            .replace("{hyperlink}", media.getMediaUrl());
                }
            }
        }
        loadTextAndImageContent(content, imageUrl);
    }


    private void loadTextAndImageContent(String content, String imageUrl) {
        if (contentView1.getVisibility() == View.INVISIBLE) {
            setText(content, textView1);
            setImage(imageUrl, imageView1);
            switchViews(contentView1, contentView2, shouldAnimate);
        } else {
            setText(content, textView2);
            setImage(imageUrl, imageView2);
            switchViews(contentView2, contentView1, shouldAnimate);
        }
    }

    private void setText(String content, TextView textView) {
        if (content.length() != 0) {
            textView.setVisibility(View.VISIBLE);
            textView.setText(Html.fromHtml(content));
            textView.setMovementMethod(LinkMovementMethod.getInstance());
        } else {
            textView.setVisibility(View.GONE);
        }
    }

    private void setImage(String imageUrl, ImageView imageView) {
        if (imageUrl != null) {
            Picasso.with(getActivity())
                    .load(imageUrl)
                    .placeholder(android.R.drawable.stat_sys_download)
                    .error(android.R.drawable.ic_dialog_alert)
                    .into(imageView);
            imageView.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.GONE);
        }
    }

    private void switchViews(View newView, View oldView, boolean shouldAnimate) {
        oldView.setVisibility(View.INVISIBLE);
        newView.setVisibility(View.VISIBLE);

        if (shouldAnimate) {
            oldView.startAnimation(exitAnimation);
            newView.startAnimation(entryAnimation);
        }
    }

    private void loadSource(String source) {
        if (source != null) {
            String sourceHtml = getActivity().getResources().getString(R.string.source_hyperlink).replace("{hyperlink}", source);
            sourceTextView.setText(Html.fromHtml(sourceHtml));
            sourceTextView.setMovementMethod(LinkMovementMethod.getInstance());
            sourceTextView.setVisibility(View.VISIBLE);
        } else {
            sourceTextView.setVisibility(View.GONE);
        }
    }

    private void loadFeed(String feed, String subFeed) {
        if (feed.equals(Item.FEED_REDDIT)) {
            feedNameTextView.setText(AppConfig.SUBREDDIT_PREFIX + subFeed);
        } else {
            feedNameTextView.setText(AppConfig.TWITTER_PREFIX + subFeed);
        }
    }

    private void hideLoading() {
        Timber.d("hideLoading");
    }

    @SuppressWarnings("unused") // actually used by ButterKnife
    @OnTouch({R.id.dum_text_view1,
            R.id.dum_text_view2,
            R.id.dum_image_view1,
            R.id.dum_image_view2})
    boolean onTouch(MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }

    @SuppressWarnings("unused") // actually used by ButterKnife
    @OnTouch(R.id.dum_fragment_root_view)
    boolean onTouchRoot(MotionEvent event) {
        return !gestureDetector.onTouchEvent(event);
    }

    @SuppressWarnings("unused") // actually used by ButterKnife
    @OnClick({R.id.dum_image_view1, R.id.dum_image_view2})
    void onImageClick(ImageView imageView) {
        Timber.d("onImageClick");
        if (exitAnimation.hasEnded()) {
            String imageUrl = null;
            for (Media media : presenter.getContent().getMedia()) {
                if (media.getType().equals(Media.TYPE_PHOTO)) {
                    imageUrl = media.getMediaUrl();
                    break;
                }
            }
            activityCallbacks.showImage(imageView, imageUrl);
        }
    }

    @Override
    public void onSwipe(SwipeListener.Direction direction) {
        Timber.d("onSwipe " + direction);
        setAnimations(direction);
        loadNewItem();
    }

    private void setAnimations(SwipeListener.Direction direction) {
        switch (direction) {
            case LEFT:
                entryAnimation = animationUtil.getSlideInRightToLeftAnimation();
                exitAnimation = animationUtil.getSlideOutRightToLeftAnimation();
                break;
            case RIGHT:
                entryAnimation = animationUtil.getSlideInLeftToRightAnimation();
                exitAnimation = animationUtil.getSlideOutLeftToRightAnimation();
                break;
            case DOWN:
                entryAnimation = animationUtil.getSlideInTopToBottomAnimation();
                exitAnimation = animationUtil.getSlideOutTopToBottomAnimation();
                break;
            case UP:
                entryAnimation = animationUtil.getSlideInBottomToTopAnimation();
                exitAnimation = animationUtil.getSlideOutBottomToTopAnimation();
                break;

        }
    }

    @SuppressWarnings("all") //actually used by EventBus
    public void onEventMainThread(FetchFeedCompletedEvent event) {
        Timber.d("onFetchFeedCompletedEvent");
        hideLoading();
        if (event.getException() != null) {
            Timber.d("exception " + event.getException()); // TODO
            event.getException().printStackTrace();
            return;
        }
        if (itemId == null || itemId == 0) {
            loadNewItem();
        }
    }

    @Override
    public void showLoading() {
        Timber.d("showLoading");
    }

    public interface ActivityCallbacks {
        public void showImage(ImageView imageView, String src);
    }

}
