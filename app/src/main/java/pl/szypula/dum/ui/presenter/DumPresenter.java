package pl.szypula.dum.ui.presenter;

import java.sql.SQLException;

import javax.inject.Inject;

import pl.szypula.dum.App;
import pl.szypula.dum.AppConfig;
import pl.szypula.dum.db.facade.ItemFacade;
import pl.szypula.dum.db.model.Item;
import pl.szypula.dum.exception.EmptyDatabaseException;
import pl.szypula.dum.manager.RedditManager;
import pl.szypula.dum.manager.TwitterManager;
import pl.szypula.dum.ui.presenter.interfaces.DumPresenterInterface;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class DumPresenter {

    private App app;
    private ItemFacade itemFacade;
    private Item item;

    private DumPresenterInterface presenterInterface;

    @Inject TwitterManager twitterManager;
    @Inject RedditManager redditManager;

    @Inject
    public DumPresenter(App app, ItemFacade itemFacade) {
        this.app = app;
        this.itemFacade = itemFacade;
    }

    public void setPresenterInterface(DumPresenterInterface presenterInterface) {
        this.presenterInterface = presenterInterface;
    }

    public Item getContent() {
        if (item == null) {
            return null;
        }
        return item;
    }

    public void reloadItem(Long itemId) {
        Timber.d("reloadItem " + itemId);
        try {
            item = itemFacade.getById(itemId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Long loadNewRandomItem() {
        Timber.d("loadNewRandomItem");
        try {
            markItemSeen();
            item = itemFacade.getRandomUnseenItem();
            if (!app.isRequestInProgress()) {
                fetchMoreItemsIfNeeded(true);
            }
            if (item != null) {
                Timber.d("item: " + item);
            }
            return (item != null) ? item.getId() : null;
        } catch (EmptyDatabaseException e) {
            Timber.d("EmptyDatabaseException, " + e);
            presenterInterface.showLoading();
            fetchAllFeeds(true, true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    private void markItemSeen() throws SQLException {
        Timber.d("markItemSeen");
        if (item != null) {
            item.setSeen(true);
            itemFacade.createOrUpdate(item);
        }
    }

    private void fetchAllFeeds(boolean sendCompleteEvent, boolean newest) {
        twitterManager.fetchFeeds(sendCompleteEvent, newest);
        redditManager.fetchFeeds(sendCompleteEvent, newest);
    }

    private void fetchMoreItemsIfNeeded(boolean newest) {
        Timber.d("fetchMoreItemsIfNeeded " + newest);
        itemFacade.getCountOfUnseenItemsObservable()
                .subscribeOn(Schedulers.newThread())
                .filter(unseenCount -> unseenCount < AppConfig.MIN_UNSEEN_COUNT)
                .subscribe(shouldFetchMoreItems -> fetchAllFeeds(false, newest), throwable -> {
                }, () -> {
                });
    }
}