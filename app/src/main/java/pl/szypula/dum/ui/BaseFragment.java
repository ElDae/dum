package pl.szypula.dum.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import icepick.Icepick;
import pl.szypula.dum.App;
import pl.szypula.dum.util.EventBusUtil;
import timber.log.Timber;

public abstract class BaseFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Timber.d("Created view for fragment: " + this.toString());
        View rootView = inflater.inflate(getLayoutId(), container, false);
        ButterKnife.inject(this, rootView);
        EventBusUtil.register(this);
        onAfterCreateView();

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
        onAfterActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (shouldSaveInstanceState()) {
            Icepick.saveInstanceState(this, outState);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        EventBusUtil.unregister(this);
    }

    protected abstract int getLayoutId();

    protected abstract void onAfterCreateView();

    protected abstract void onAfterActivityCreated(@Nullable Bundle savedInstanceState);

    protected abstract boolean shouldSaveInstanceState();
}
