package pl.szypula.dum.ui.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewWithCustomFont extends TextView {
    public TextViewWithCustomFont(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextViewWithCustomFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewWithCustomFont(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/DroidSerif-Regular.ttf");
        setTypeface(tf ,1);

    }
}
