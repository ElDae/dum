package pl.szypula.dum.network.response.reddit;

import java.util.List;

import lombok.Getter;
import lombok.ToString;

@Getter @ToString
public class ListingData {
    private List<Child> children;

    public String getDisplayName() {
        if (children != null && !children.isEmpty()) {
            return children.get(0).getDisplayName();
        }
        return null;
    }

    public String getUrl() {
        if (children != null && !children.isEmpty()) {
            return children.get(0).getUrl();
        }
        return null;
    }
}
