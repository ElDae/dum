package pl.szypula.dum.network.response.twitter;

import lombok.Getter;
import lombok.ToString;

@Getter @ToString
public class TwitterTokenResponse {

    private String tokenType;
    private String accessToken;

    public TwitterTokenResponse(String accessToken) {
        this.accessToken = accessToken;
    }
}
