package pl.szypula.dum.network;

import pl.szypula.dum.network.response.reddit.InfoResponse;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

public interface RedditRestInterface {

    @GET("/api/info.json")
    Observable<InfoResponse> subredditInfo(@Query("id") String subredditId);

    @GET("/r/{displayName}/new/.json")
    Observable<InfoResponse> subredditFeed(@Path("displayName") String displayName,
                                           @Query("after") String after,
                                           @Query("before") String before,
                                           @Query("order") String order,
                                           @Query("limit") int limit);
}
