package pl.szypula.dum.network;

import retrofit.Endpoint;

public class CustomEndpoint implements Endpoint {
    private String url;
    private String name;

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
