package pl.szypula.dum.network.response.twitter;

import lombok.Getter;
import lombok.ToString;

@Getter @ToString
public class User {
    private String screenName;
}
