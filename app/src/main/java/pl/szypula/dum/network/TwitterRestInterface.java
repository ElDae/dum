package pl.szypula.dum.network;

import java.util.List;

import pl.szypula.dum.network.response.twitter.Tweet;
import pl.szypula.dum.network.response.twitter.TwitterTokenResponse;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Query;
import rx.Observable;

public interface TwitterRestInterface {

    @FormUrlEncoded
    @POST("/oauth2/token")
    public Observable<TwitterTokenResponse> getBearerToken(@Header("Authorization") String authorization,
                                                           @Header("Content-Type") String contentType,
                                                           @Field("grant_type") String grantType);

    @GET("/1.1/statuses/user_timeline.json")
    public Observable<List<Tweet>> getTimeLine(@Header("Authorization") String authorization,
                                               @Query("user_id") String userId,
                                               @Query("screen_name") String screenName,
                                               @Query("count") int count,
                                               @Query("max_id") Long id);
}
