package pl.szypula.dum.network.response.twitter;

import java.util.List;

import lombok.Getter;
import lombok.ToString;

@Getter @ToString
public class Entities {
    private List<Media> media;
    private List<Urls> urls;
}
