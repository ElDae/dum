package pl.szypula.dum.network;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.DateTypeAdapter;
import com.squareup.okhttp.OkHttpClient;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import pl.szypula.dum.AppConfig;
import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public class RestAdapterInvoker {

    private final OkHttpClient okHttpClient;
    private CustomEndpoint customEndpoint;
    private RestAdapter restAdapter;

    private RedditRestInterface redditRestInterface;
    private TwitterRestInterface twitterRestInterface;

    public RestAdapterInvoker(OkHttpClient okHttpClient, CustomEndpoint customEndpoint) {
        this.okHttpClient = okHttpClient;
        this.customEndpoint = customEndpoint;
    }

    public RedditRestInterface getRedditRestInterface() {
        customEndpoint.setUrl(AppConfig.REDDIT_ENDPOINT_URL);
        initRestAdapter();
        initRedditRestInterface();

        return redditRestInterface;
    }

    public TwitterRestInterface getTwitterRestInterface() {
        customEndpoint.setUrl(AppConfig.TWITTER_ENDPOINT_URL);
        initRestAdapter();
        initTwitterRestInterface();

        return twitterRestInterface;
    }

    private void initRestAdapter() {
        if (restAdapter == null) {
            restAdapter = createRestAdapter();
        }
    }

    private RestAdapter createRestAdapter() {
        okHttpClient.setConnectTimeout(30, TimeUnit.SECONDS);

        RestAdapter.LogLevel logLevel = RestAdapter.LogLevel.FULL;

        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        RestAdapter.Builder builder = new RestAdapter.Builder();
        builder.setClient(new OkClient(okHttpClient))
                .setLogLevel(logLevel)
                .setConverter(new GsonConverter(gson))
                .setEndpoint(customEndpoint)
                .setErrorHandler(myErrorHandler);

        return builder.build();
    }

    private void initRedditRestInterface() {
        if (redditRestInterface == null) {
            redditRestInterface = restAdapter.create(RedditRestInterface.class);
        }
    }

    private void initTwitterRestInterface() {
        if (twitterRestInterface == null) {
            twitterRestInterface = restAdapter.create(TwitterRestInterface.class);
        }
    }

    private static ErrorHandler myErrorHandler = new ErrorHandler() {
        @Override
        public Throwable handleError(RetrofitError cause) {
            cause.printStackTrace();
            return cause;
        }
    };
}
