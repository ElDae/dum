package pl.szypula.dum.network.response.twitter;


import lombok.Getter;
import lombok.ToString;

@ToString @Getter
public class Tweet {
    private String createdAt;
    private long id;
    private String idStr;
    private String text;
    private User user;
    private Entities entities;
}
