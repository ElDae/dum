package pl.szypula.dum.network.response.twitter;

import lombok.Getter;
import lombok.ToString;

@Getter @ToString
public class Media {
    private String mediaUrl;
    private String mediaUrlHttps;
    private String url;
    private String type;
}
