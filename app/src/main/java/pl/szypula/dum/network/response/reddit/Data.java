package pl.szypula.dum.network.response.reddit;

import lombok.Getter;
import lombok.ToString;

@Getter @ToString
public class Data {
    private String id;
    private String url;
    private String title;
    private String displayName;
    private String subreddit;
    private String permalink;

    private boolean isSelf;
    private String thumbnail;
    private boolean over18;
    private long created;
    private long createdUtc;
}
