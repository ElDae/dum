package pl.szypula.dum.network.response.twitter;

import lombok.Getter;
import lombok.ToString;

@Getter @ToString
public class Urls {
    private String url;
    private String expandedUrl;
    private String displayUrl;
}
