package pl.szypula.dum.network.response.reddit;

import lombok.Getter;
import lombok.ToString;

@Getter @ToString
public class Child {
    private Data data;

    public String getDisplayName() {
        return data.getDisplayName();
    }

    public String getUrl() {
        return data.getUrl();
    }

    public boolean isValidItem() {
        return !data.isSelf();
    }

}
