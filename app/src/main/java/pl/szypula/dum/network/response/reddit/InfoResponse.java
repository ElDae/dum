package pl.szypula.dum.network.response.reddit;

import lombok.Getter;
import lombok.ToString;

@Getter @ToString
public class InfoResponse {
    private ListingData data;

    public String getDisplayName() {
        return getUrl().split("/")[2];
    }

    public String getUrl() {
        return data.getUrl();
    }

}
