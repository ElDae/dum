package pl.szypula.dum.db.model;

import com.j256.ormlite.field.DatabaseField;

public abstract class AbstractModel {

    protected AbstractModel() {
    }

    public static final String FIELD_ID = "id";

    @DatabaseField(generatedId = true, columnName = FIELD_ID)
    protected long id;

    public long getId() {
        return id;
    }
}
