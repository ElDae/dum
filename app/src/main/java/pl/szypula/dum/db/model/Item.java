package pl.szypula.dum.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.text.ParseException;
import java.util.Collection;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.szypula.dum.AppConfig;
import pl.szypula.dum.network.response.reddit.Child;
import pl.szypula.dum.network.response.twitter.Tweet;
import pl.szypula.dum.util.CalendarUtil;

@DatabaseTable
@Getter @Setter @ToString
public class Item extends AbstractModel {
    public final static String FIELD_EXTERNAL_ID = "external_id";
    public final static String FIELD_CONTENT = "content";
    public final static String FIELD_URL = "url";
    public final static String FIELD_SRC = "src";
    public final static String FIELD_THUMBNAIL = "thumbnail";
    public final static String FIELD_CREATION = "creation";
    public final static String FIELD_SEEN = "seen";
    public final static String FIELD_MARKED = "marked";
    public final static String FIELD_FEED = "feed";
    public final static String FIELD_SUB_FEED = "sub_feed";
    public final static String FIELD_MEDIA = "media";

    public static final String FEED_REDDIT = "Reddit";
    public static final String FEED_TWITTER = "Twitter";

    @DatabaseField(columnName = FIELD_EXTERNAL_ID, unique = true)
    private String externalId;
    @DatabaseField(columnName = FIELD_CONTENT)
     private String content;
    @DatabaseField(columnName = FIELD_URL)
    private String url;
    @DatabaseField(columnName = FIELD_SRC)
    private String src;
    @DatabaseField(columnName = FIELD_THUMBNAIL)
    private String thumbnail;
    @DatabaseField(columnName = FIELD_CREATION)
    private long creation;
    @DatabaseField(columnName = FIELD_SEEN)
    private boolean seen;
    @DatabaseField(columnName = FIELD_MARKED)
    private boolean marked;
    @DatabaseField(columnName = FIELD_FEED)
    private String feed;
    @DatabaseField(columnName = FIELD_SUB_FEED)
    private String subFeed;
    @ForeignCollectionField(columnName = FIELD_MEDIA)
    private Collection<Media> media;

    public Item() {
    }

    public Item(Child child) {
        this.externalId = child.getData().getId();
        this.content = child.getData().getTitle();
        this.url = AppConfig.REDDIT_URL + child.getData().getPermalink();
        this.src = child.getData().getUrl();
        this.thumbnail = child.getData().getThumbnail();
        this.creation = child.getData().getCreatedUtc();
        this.subFeed = child.getData().getSubreddit();
        this.feed = FEED_REDDIT;
        this.seen = false;
        this.marked = false;
    }

    public Item(Tweet tweet) {
        this.externalId = String.valueOf(tweet.getId());
        this.content = tweet.getText();
        try {
            this.creation = CalendarUtil.getTwitterDateFormat().parse(tweet.getCreatedAt()).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.subFeed = tweet.getUser().getScreenName();
        this.feed = FEED_TWITTER;
        this.seen = false;
        this.marked = false;
    }
}
