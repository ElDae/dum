package pl.szypula.dum.db.facade;

import com.j256.ormlite.dao.Dao;

import pl.szypula.dum.db.model.Media;

public class MediaFacade extends AbstractFacade<Media> {
    public MediaFacade(Dao<Media, Long> dao) {
        super(dao);
    }
}
