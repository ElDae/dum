package pl.szypula.dum.db.facade;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

import pl.szypula.dum.db.model.Item;
import pl.szypula.dum.exception.EmptyDatabaseException;
import rx.Observable;
import timber.log.Timber;

public class ItemFacade extends AbstractFacade<Item> {
    public ItemFacade(Dao<Item, Long> dao) {
        super(dao);
    }

    public Item getRandomUnseenItem() throws SQLException {
        QueryBuilder<Item, Long> queryBuilder = getQueryBuilder();
        queryBuilder.orderByRaw("RANDOM()").limit(1l).where().eq(Item.FIELD_SEEN, false);

        List<Item> items = dao.query(queryBuilder.prepare());
        if (!items.isEmpty()) {
            return items.get(0);
        } else {
            if (getCountOf() != 0) {
                return null;
            } else {
                throw new EmptyDatabaseException();
            }
        }
    }

    public Item getByExternalId(String externalId) throws SQLException {
        List<Item> items = dao.queryForEq(Item.FIELD_EXTERNAL_ID, externalId);
        if (items != null && !items.isEmpty()) {
            Timber.d("item with id " + externalId + " already exists");
            return items.get(0);
        } else {
            Timber.d("no item with id " + externalId + " - saving");
        }
        return null;
    }

    public long getCountOfUnseen() throws SQLException {
        QueryBuilder<Item, Long> queryBuilder = getQueryBuilder();
        return queryBuilder.where().eq(Item.FIELD_SEEN, false).countOf();
    }

    public long getCountOfUnseenForFeed(String feed, String subFeed) throws SQLException {
        QueryBuilder<Item, Long> queryBuilder = getQueryBuilder();
        return queryBuilder.where().eq(Item.FIELD_SEEN, false)
                .and().eq(Item.FIELD_FEED, feed)
                .and().eq(Item.FIELD_SUB_FEED, subFeed)
                .countOf();
    }

    public Item getNewestItem() throws SQLException {
        Timber.d("getNewestItem");
        QueryBuilder<Item, Long> queryBuilder = getQueryBuilder();
        queryBuilder.orderBy(Item.FIELD_CREATION, false).limit(1l);
        return dao.queryForFirst(queryBuilder.prepare());
    }

    public Item getNewestMarkedItemForFeed(String feed, String subFeed) throws SQLException {
        Timber.d("getNewestMarkedItemForFeed " + feed);
        QueryBuilder<Item, Long> queryBuilder = getQueryBuilder();
        queryBuilder.orderBy(Item.FIELD_CREATION, false).limit(1l)
                .where().eq(Item.FIELD_MARKED, true)
                .and().eq(Item.FIELD_FEED, feed)
                .and().eq(Item.FIELD_SUB_FEED, subFeed);
        try {
            return dao.queryForFirst(queryBuilder.prepare());
        } catch (SQLException e) {
            Timber.d("exception,  " + e);
            e.printStackTrace();
            return null;
        }
    }

    public Item getOldestItemForFeed(String feed, String subFeed) throws SQLException {
        Timber.d("getOldestItemForFeed " + feed);
        QueryBuilder<Item, Long> queryBuilder = getQueryBuilder();
        queryBuilder.orderBy(Item.FIELD_CREATION, true).limit(1l)
                .where().eq(Item.FIELD_FEED, feed)
                .and().eq(Item.FIELD_SUB_FEED, subFeed);
        return dao.queryForFirst(queryBuilder.prepare());
    }

    public boolean create(Item item) throws SQLException {
        if (getByExternalId(item.getExternalId()) == null) {
            dao.create(item);
            return true;
        }
        return false;
    }

    /*
    Observables
     */

    public Observable<Item> getItemToStartDownloadFromForFeedObservable(boolean shouldGetOldest, String feed, String subFeed) {
        Timber.d("ItemFacade.getItemToStartDownloadFromForFeedObservable");
        return Observable.create(subscriber -> {
            try {
                Item newestMarkedItem = getNewestMarkedItemForFeed(feed, subFeed);
                if (newestMarkedItem != null) {
                    newestMarkedItem.setMarked(false);
                    createOrUpdate(newestMarkedItem);
                } else if (shouldGetOldest) {
                    newestMarkedItem = getOldestItemForFeed(feed, subFeed);
                }
                subscriber.onNext(newestMarkedItem);
                subscriber.onCompleted();
            } catch (SQLException e) {
                subscriber.onError(e);
            }
        });
    }

    public Observable<Long> getCountOfUnseenItemsObservable() {
        return Observable.create(subscriber -> {
            try {
                long countOfUnseen = getCountOfUnseen();
                Timber.d("count of unseen items: " + countOfUnseen);
                subscriber.onNext(countOfUnseen);
                subscriber.onCompleted();
            } catch (SQLException e) {
                subscriber.onError(e);
            }
        });
    }

    public Observable<Long> getCountOfUnseenItemsForFeedObservable(String feed, String subFeed) {
        return Observable.create(subscriber -> {
            try {
                long countOfUnseen = getCountOfUnseenForFeed(feed, subFeed);
                Timber.d("count of unseen items for feed " + feed + " " + subFeed + ": " + countOfUnseen);
                subscriber.onNext(countOfUnseen);
                subscriber.onCompleted();
            } catch (SQLException e) {
                subscriber.onError(e);
            }
        });
    }
}
