package pl.szypula.dum.db.facade;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public class AbstractFacade<T> {

    protected Dao<T, Long> dao;

    public AbstractFacade(Dao<T, Long> dao) {
        this.dao = dao;
    }

    public T refresh(T objectToRefresh) throws SQLException {
        if (objectToRefresh.getClass() != dao.getDataClass()) {
            throw new ClassCastException("Dao data type doesn't match object class. Check whether you used correct facade");
        }
        dao.refresh(objectToRefresh);
        return objectToRefresh;
    }

    public void createOrUpdate(T data) throws SQLException {
        dao.createOrUpdate(data);
    }

    public void createOrUpdate(Collection<T> dataCollection) throws SQLException {
        for (T object : dataCollection) {
            createOrUpdate(object);
        }
    }

    public void deleteById(Long id) throws SQLException {
        dao.delete(getById(id));
    }

    public void delete(T data) throws SQLException {
        dao.delete(data);
    }

    public void delete(Collection<T> data) throws SQLException {
        dao.delete(data);
    }

    public T getById(Long id) throws SQLException {
        return dao.queryForId(id);
    }

    public List<T> getAll() throws SQLException {
        return dao.queryForAll();
    }

    public long getCountOf() throws SQLException {
        return dao.countOf();
    }

    protected QueryBuilder<T, Long> getQueryBuilder() throws SQLException {
        return dao.queryBuilder();
    }
}