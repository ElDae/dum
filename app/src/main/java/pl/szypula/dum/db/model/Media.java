package pl.szypula.dum.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.szypula.dum.network.response.twitter.Urls;

@DatabaseTable
@Getter @Setter @ToString
public class Media extends AbstractModel {

    public static final String FIELD_URL = "url";
    public static final String FIELD_MEDIA_URL = "media_url";
    public static final String FIELD_TYPE = "type";
    public static final String FIELD_ITEM = "item_id";

    public static final String TYPE_URL = "url";
    public static final String TYPE_PHOTO = "photo";

    @DatabaseField(columnName = FIELD_URL)
    private String url;
    @DatabaseField(columnName = FIELD_MEDIA_URL)
    private String mediaUrl;
    @DatabaseField(columnName = FIELD_TYPE)
    private String type;
    @DatabaseField(columnName = FIELD_ITEM, foreign = true, foreignAutoRefresh = true, canBeNull = false)
    private Item item;

    private Media() {}

    public  Media(Item item, pl.szypula.dum.network.response.twitter.Media media) {
        this.item = item;
        this.url = media.getUrl();
        this.mediaUrl = media.getMediaUrl();
        this.type = media.getType();
    }

    public Media(Item item, Urls urls) {
        this.item = item;
        this.url = urls.getUrl();
        this.mediaUrl = urls.getExpandedUrl();
        this.type = TYPE_URL;
    }
}
