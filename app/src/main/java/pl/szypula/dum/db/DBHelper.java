package pl.szypula.dum.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import pl.szypula.dum.AppConfig;
import pl.szypula.dum.db.model.Item;
import pl.szypula.dum.db.model.Media;
import timber.log.Timber;

public class DBHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = AppConfig.DATABASE_NAME;
    private static final int DATABASE_VERSION = 1;

    private Map<Class, Dao> daoMap = new HashMap<Class, Dao>();

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        createClassesList();
    }

    //put all database classes here
    private void createClassesList() {
        daoMap.put(Item.class, null);
        daoMap.put(Media.class, null);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        Timber.d("DBHelper.onCreate");
        try {
            for (Class aClass : daoMap.keySet()) {
                TableUtils.createTable(connectionSource, aClass);
            }
        } catch (SQLException e) {
            Timber.d("Exception while creating db: " + e);
            throw new RuntimeException("Exception while creating db");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            //upgrade database
        }
    }

    public <T> Dao getModelDao(Class<T> clz) throws SQLException {
        Dao dao = daoMap.get(clz);
        if (dao == null) {
            dao = getDao(clz);
            daoMap.put(clz, dao);
        }
        return dao;
    }

    public Set<Class> getDaoClassesList() {
        return daoMap.keySet();
    }
}
