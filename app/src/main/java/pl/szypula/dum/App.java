package pl.szypula.dum;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;

import com.facebook.stetho.Stetho;

import java.util.Arrays;
import java.util.List;

import dagger.ObjectGraph;
import pl.szypula.dum.modules.AndroidModule;
import pl.szypula.dum.modules.AnimationModule;
import pl.szypula.dum.modules.PersistanceModule;
import pl.szypula.dum.modules.RestApiModule;
import pl.szypula.dum.ui.BaseActivity;
import rx.Observable;
import timber.log.Timber;

/**
 * Author Piotr Szypuła on 2014-07-16.
 */
public class App extends Application {
    private ObjectGraph graph;
    private BaseActivity activity;
    private volatile boolean isRequestInProgress = false;

    @Override
    public void onCreate() {
        super.onCreate();

        graph = ObjectGraph.create(getModules().toArray());
        Timber.plant(new Timber.DebugTree());
        log();
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                        .build());
    }

    protected List<Object> getModules() {
        return Arrays.asList(
                new AndroidModule(this),
                new PersistanceModule(),
                new RestApiModule(),
                new AnimationModule()
        );
    }

    public void inject(Object object) {
        graph.inject(object);
    }

    @SuppressWarnings("all")
    private void log() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            Timber.i("APPLICATION CREATED!");
            Timber.i(getPackageName() + ": " + getString(getApplicationInfo().labelRes));
            Timber.i("Version name: " + pInfo.versionName);
            Timber.i("Version code: " + pInfo.versionCode);
        } catch (Exception e) {
            //nth to do
        }
    }

    public void setCurrentActivity(BaseActivity baseActivity) {
        this.activity = baseActivity;
    }

    public BaseActivity getCurrentActivity() {
        return activity;
    }

    public SharedPreferences getSharedPreferences() {
        return getSharedPreferences(AppConfig.DUM_PREFS_NAME, Context.MODE_PRIVATE);
    }

    public Observable<String> getTwitterBearerObservable() {
        return Observable.just(getSharedPreferences().getString(AppConfig.PREFS_TWITTER_BEARER, null));
    }

    public boolean isRequestInProgress() {
        return isRequestInProgress;
    }

    public void setRequestInProgress(boolean isRequestInProgress) {
        this.isRequestInProgress = isRequestInProgress;
    }
}
