package pl.szypula.dum.util;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import lombok.Getter;
import pl.szypula.dum.R;

@Getter
public class AnimationUtil {

    private Animation slideInLeftToRightAnimation;
    private Animation slideOutLeftToRightAnimation;
    private Animation slideInRightToLeftAnimation;
    private Animation slideOutRightToLeftAnimation;
    private Animation slideInTopToBottomAnimation;
    private Animation slideOutTopToBottomAnimation;
    private Animation slideInBottomToTopAnimation;
    private Animation slideOutBottomToTopAnimation;

    public AnimationUtil(Context context) {
        slideInLeftToRightAnimation = AnimationUtils.loadAnimation(context, R.anim.left_to_right_slide_in);
        slideOutLeftToRightAnimation = AnimationUtils.loadAnimation(context, R.anim.left_to_right_slide_out);

        slideInRightToLeftAnimation = AnimationUtils.loadAnimation(context, R.anim.right_to_left_slide_in);
        slideOutRightToLeftAnimation = AnimationUtils.loadAnimation(context, R.anim.right_to_left_slide_out);

        slideInTopToBottomAnimation = AnimationUtils.loadAnimation(context, R.anim.top_to_bottom_slide_in);
        slideOutTopToBottomAnimation = AnimationUtils.loadAnimation(context, R.anim.top_to_bottom_slide_out);

        slideInBottomToTopAnimation = AnimationUtils.loadAnimation(context, R.anim.bottom_to_top_slide_in);
        slideOutBottomToTopAnimation = AnimationUtils.loadAnimation(context, R.anim.bottom_to_top_slide_out);
    }
}
