package pl.szypula.dum.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import pl.szypula.dum.R;
import pl.szypula.dum.ui.DumFragment;
import timber.log.Timber;

public class FragmentNavigationUtil {
    private final FragmentManager fragmentManager;

    public FragmentNavigationUtil(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public void showDumFragment() {
        DumFragment fragment = DumFragment.newInstance();
        showFragment(fragment);
    }

    private void showFragment(Fragment fragment) {
        String tag = ((Object) fragment).getClass().getSimpleName();
        Timber.d("FragmentNavigationUtil, showFragment " + tag);
        String lastFragmentName = "";

        if (fragmentManager.getBackStackEntryCount() > 0) {
            lastFragmentName = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
        }

        if (!lastFragmentName.equals(tag)) {
            boolean fragmentPopped = fragmentManager.popBackStackImmediate(tag, 0);
            if (!fragmentPopped) {
                fragmentManager.beginTransaction()
                        //.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.container, fragment, tag)
                        .addToBackStack(tag)
                        .commit();
            }
        }
    }
}
