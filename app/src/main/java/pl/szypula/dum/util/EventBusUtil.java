package pl.szypula.dum.util;

import de.greenrobot.event.EventBus;
import lombok.experimental.UtilityClass;

@UtilityClass
public class EventBusUtil {
    public static void register(Object object) {
        boolean isRegistered = EventBus.getDefault().isRegistered(object);
        if (!isRegistered) {
            EventBus.getDefault().register(object);
        }
    }

    public static void unregister(Object object) {
        boolean isRegistered = EventBus.getDefault().isRegistered(object);
        if (isRegistered) {
            EventBus.getDefault().unregister(object);
        }
    }
}
