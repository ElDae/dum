package pl.szypula.dum.util;

import java.text.SimpleDateFormat;
import java.util.Locale;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CalendarUtil {

    private static SimpleDateFormat twitterDateFormat;

    public static SimpleDateFormat getTwitterDateFormat() {
        if (twitterDateFormat == null) {
            twitterDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.US);
        }
        return twitterDateFormat;
    }

}
