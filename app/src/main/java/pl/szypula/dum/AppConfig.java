package pl.szypula.dum;

import lombok.experimental.UtilityClass;

@UtilityClass
public class AppConfig {
    // Database
    public static final String DATABASE_NAME = "dumdb";

    // Endpoints
    public static final String REDDIT_ENDPOINT_URL = "http://www.reddit.com";
    public static final String TWITTER_ENDPOINT_URL = "https://api.twitter.com";

    // Reddit
    public static final String REDDIT_ITEM_PREFIX = "t3_";
    public static final String SUBREDDIT_PREFIX = "r/";
    public static final String REDDIT_URL = "www.reddit.com";
    public static final String REDDIT_ORDER = "new";
    public static final String R_TIL_ID = "t5_2qqjc";

    // Twitter
    // bearer_token = base64("consumer_key:Consumer_secret")
    public static final String TWITTER_PREFIX = "@";
    public static final String TWITTER_GOOGLE_FACTS_USER_ID = "559675462";
    public static final String TWITTER_GOOGLE_FACTS_SCREEN_NAME = "GoogleFacts";

    // SharedPreferences
    public static final String DUM_PREFS_NAME = "dum_shared_preferences";
    public static final String PREFS_TWITTER_BEARER = "twitter_bearer";

    // Requests
    public static final int ITEM_FETCH_LIMIT = 20; // TODO: change to 100?
    public static final int MIN_UNSEEN_COUNT = 10; // TODO: more, like 20?
}
