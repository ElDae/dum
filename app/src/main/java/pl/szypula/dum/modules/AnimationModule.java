package pl.szypula.dum.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.szypula.dum.App;
import pl.szypula.dum.ui.DumFragment;
import pl.szypula.dum.util.AnimationUtil;


@Module(injects = {
        DumFragment.class,
}, includes = AndroidModule.class, complete = false, library = true)
public class AnimationModule {

    @Provides
    @Singleton
    public AnimationUtil providesAnimationUtil(App application) {
        return new AnimationUtil(application);
    }
}
