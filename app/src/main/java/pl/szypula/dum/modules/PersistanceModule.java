package pl.szypula.dum.modules;

import java.sql.SQLException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.szypula.dum.App;
import pl.szypula.dum.db.DBHelper;
import pl.szypula.dum.db.facade.ItemFacade;
import pl.szypula.dum.db.facade.MediaFacade;
import pl.szypula.dum.db.model.Item;
import pl.szypula.dum.db.model.Media;
import pl.szypula.dum.ui.DumFragment;

@Module(injects = {
        DumFragment.class,
}, includes = AndroidModule.class, complete = false, library = true)
public class PersistanceModule {

    @Provides
    @Singleton
    DBHelper providesDBHelper(App application) {
        return new DBHelper(application);
    }

    @Provides
    @Singleton
    @SuppressWarnings("unchecked")
    public ItemFacade providesItemFacade(DBHelper dbHelper) {
        try {
            return new ItemFacade(dbHelper.getModelDao(Item.class));
        } catch (SQLException e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    @Provides
    @Singleton
    @SuppressWarnings("unchecked")
    public MediaFacade providesMediaFacade(DBHelper dbHelper) {
        try {
            return new MediaFacade(dbHelper.getModelDao(Media.class));
        } catch (SQLException e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }
}
