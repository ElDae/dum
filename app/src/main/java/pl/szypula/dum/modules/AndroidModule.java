package pl.szypula.dum.modules;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.szypula.dum.App;

@Module(library = true)
public class AndroidModule {
    private final App application;

    @Inject
    public AndroidModule(App application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public App providerApplicationContext() {
        return application;
    }
}
