package pl.szypula.dum.modules;

import com.squareup.okhttp.OkHttpClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.szypula.dum.network.CustomEndpoint;
import pl.szypula.dum.network.RedditRestInterface;
import pl.szypula.dum.network.RestAdapterInvoker;
import pl.szypula.dum.network.TwitterRestInterface;
import pl.szypula.dum.ui.DumActivity;
import pl.szypula.dum.ui.ImageActivity;

@Module(injects = {
        DumActivity.class,
        ImageActivity.class
}, complete = false, library = true)
public class RestApiModule {

    @Provides
    @Singleton
    OkHttpClient providesOkHttpClient() {
        return new OkHttpClient();
    }

    @Provides
    CustomEndpoint providesCustomEndpoint() {
        CustomEndpoint customEndpoint = new CustomEndpoint();
        customEndpoint.setUrl("http://");
        return customEndpoint;
    }

    @Provides
    public RestAdapterInvoker providesRestInvokerModule(OkHttpClient okHttpClient, CustomEndpoint customEndpoint) {
        return new RestAdapterInvoker(okHttpClient, customEndpoint);
    }
}
