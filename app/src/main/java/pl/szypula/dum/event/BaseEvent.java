package pl.szypula.dum.event;

public abstract class BaseEvent {
    private Throwable exception;
    private String message;

    protected BaseEvent() {
    }

    protected BaseEvent(Throwable exception) {
        this.exception = exception;
    }

    protected BaseEvent(String message) {
        this.message = message;
    }

    protected BaseEvent(Throwable exception, String message) {
        this.exception = exception;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Throwable getException() {
        return exception;
    }

    public void setException(Throwable exception) {
        this.exception = exception;
    }
}
