package pl.szypula.dum.event;

public class FetchFeedCompletedEvent extends BaseEvent {

    public FetchFeedCompletedEvent() {
    }

    public FetchFeedCompletedEvent(Throwable exception) {
        super(exception);
    }
}
