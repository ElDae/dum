# About Dum! #

Dum! is an app that collects interesting facts about world.

This project is my way of learning [Dagger](http://square.github.io/dagger) and [rxJava](https://github.com/ReactiveX/RxJava) + [retrolambda](https://github.com/evant/gradle-retrolambda). It is work in progress and I'm developing it only for educational purposes (maybe some day it will be in store, who knows). 

### What's been done ###

* Feed from [r/til](reddit.com/r/todayilearned) using Reddit REST API,
* Feed from [@GoogleFacts](https://twitter.com/googlefacts) and [@Know](https://twitter.com/know) using Twitter REST API.

### TODO ###

* Load Images (picasso? universal image loader?),
* Settings,
* Custom feeds,
* Layout (lollipop transitions).

### Additional info ###

Since I'm using retrolamba in this project Java 8 is required to compile it.